# Copyright holder
EUMETSAT
 
# List of development authors
Julia Wagemann (MEEO)
Ben Loveday (Innoflair UG)
Hayley Evers-King (EUMETSAT)

# Support
For all queries on this software package, please contact ops@eumetsat.int
